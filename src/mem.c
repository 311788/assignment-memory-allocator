#include <stdarg.h>

#define _DEFAULT_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size
size_from_capacity(block_capacity
cap); //вставляем эти функции из mem_intervals.h
extern inline block_capacity
capacity_from_size(block_size
sz); //размер на размер заголовка больше вместимости

static bool block_is_big_enough(size_t query, struct block_header *block) {
    return block->capacity.bytes >= query; //campasy- структура с одним полем bytes, query- минимум
}

static size_t pages_count(size_t mem) {
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
} //getpagesize- системный вызов, возвращающий количество байтов в одной странице
//мы ищем количество страниц, достаточное для сохранения блока (округляем вверх)
static size_t round_pages(size_t mem) {
    return getpagesize() * pages_count(mem);
}//округляем вверх по количеству страниц (выравнивание), тк должен быть пропорционален размеру страницы

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
} //?

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }
//если блок меньше минимального размера берем блок минимального размера

extern inline bool

region_is_invalid(const struct region *r); //если адрес региона NULL


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
} //мы можем подавать в функцию один из этих вариантов

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
    struct region reg;
    query = region_actual_size(query);
    void *reg_addr = map_pages(addr, query, MAP_FIXED_NOREPLACE); //выделяем вплотную (те точно по данному адресу)
    //адрес или ошибка (надо обработать)
    //Если выделить регион вплотную не получилось, то нужно выделить регион "где получится"
    if (reg_addr == MAP_FAILED) {
        reg_addr = map_pages(addr, query, false);
        if (reg_addr == MAP_FAILED) return NULL; //если и где получится не вышло
    }
    block_init(reg_addr, (block_size) {query}, NULL); //block_size- структура
    return (struct region) {reg_addr, query, false}
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}  //создание кучи

#define BLOCK_MIN_CAPACITY 24 //минимальный размер блока (размер заголовка)

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block,
                             size_t query) { //restrict обозначает, что на этот блок памяти только 1 указатель
    return block->is_free && query + offsetof(
    struct block_header, contents) +BLOCK_MIN_CAPACITY <= block->capacity.bytes;
    //если блок свободен и его размер больше либо равен чем необходимый размер (query+размер заголовка) и также останется блок больше,
    //чем размер минимального, то мы должны поделить
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    if (block_splittable(block, query)) { //? мы уже проверили блок или нет
        struct block_header next_block = ((struct block_header) {.is_free=true, .capacity=block->capacity.bytes -
                                                                                          query, .next=block->next});
        *block = (struct block_header) {.is_free=true, .capacity=round_pages(query), .next=block->contents + query};
        return true;
    }
    return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents +
                     block->capacity.bytes); //те адрес начала блока+ его вместимость= адрес для заголовка следующего блока
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
} //то что второй блок после первого

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
} //что блоки соединяемы

static bool try_merge_with_next(struct block_header *block) {
    struct block_header *next = block->next;
    if (mergeable(block, next)) {
        *block = (struct block_header) {.is_free=true, .capacity=block->capacity.bytes +
                                                                 next->capacity.bytes, .next=next->next};
        return true;
    } else return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    // Прежде чем решать, хороший блок или нет, объединим его со всеми идущими за ним свободными блоками.
    while (try_merge_with_next(block));
    if (block) {
        struct block_header *last_block = block;
        while (block) {
            if (block_is_big_enough(sz, block) && block->is_free) {
                return (struct block_search_result) {BSR_FOUND_GOOD_BLOCK, block};
            }
            last_block = block;
            block = block->next;
        }
        return (struct block_search_result) {BSR_REACHED_END_NOT_FOUND, last_block};
    }
    return (struct block_search_result) {BSR_CORRUPTED, NULL};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
//    Перебираем блоки пока не находим "хороший". Хороший блок — такой, в который можно уместить n байт.
    struct block_search_result good_or_last = find_good_or_last(block, query);
    if (good_or_last.type == BSR_FOUND_GOOD_BLOCK) {
        //Хороший блок может быть слишком большим, тогда мы разделяем блок на две части:
        split_if_too_big(good_or_last.block, query);//не проверяем, тк внутри есть проверка
        // Адрес содержимого этого блока и вернёт malloc.
        return (struct block_search_result) {BSR_FOUND_GOOD_BLOCK, good_or_last.block};
    } else if (good_or_last.type == BSR_REACHED_END_NOT_FOUND)
        return good_or_last;
    return (struct block_search_result) {BSR_CORRUPTED, NULL};
}


static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    struct region reg = alloc_region(last, query);
    last->next = reg.addr;
    try_merge_with_next(last);
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    if (query < BLOCK_MIN_CAPACITY) {
        return memalloc(BLOCK_MIN_CAPACITY, heap_start);
    }
    struct block_search_result try_memalloc = try_memalloc_existing(query, heap_start);
    if (try_memalloc.type == BSR_FOUND_GOOD_BLOCK) {
        return try_memalloc.block;
    } else if (try_memalloc.type == BSR_REACHED_END_NOT_FOUND) {
        grow_heap(try_memalloc.block, query);
        memalloc(query, heap_start);
    }
    return NULL;
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
} //либо адрес либо null

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(
    struct block_header, contents));
}//по contest получаем block_header (те адрес начала заголовка)

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header));
}