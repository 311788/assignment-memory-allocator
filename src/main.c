#include "src/mem.h"
int first_test() {
    int64_t *list = _malloc(100);
    if (list) {
        printf("Test1: OK");
    }
}
//Освобождение одного блока из нескольких выделенных.
int second_test() {
    int64_t *list = _malloc(100);
    int64_t *list2 = _malloc(200);
    _free(list);
    printf("Test2: OK");
}
//Освобождение двух блоков из нескольких выделенных.
int third_test() {
    int64_t *list = _malloc(100);
    int64_t *list2 = _malloc(200);
    int64_t *list3 = _malloc(200);
    _free(list);
    _free(list2);
    printf("Test3: OK");
}
//Память закончилась, новый регион памяти расширяет старый.
int fourth_test() {
    int64_t *list = _malloc(100);
    int64_t *list2 = _malloc(200);
    int64_t *list3 = _malloc(300);
    printf("Test4: OK");
}
//Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
int fifth_test() {
    int64_t *list = _malloc(100);
    int64_t *list2 = _malloc(200);
    heap_init(300);
    int64_t *list3 = _malloc(300);
    printf("Test5: OK");
}

int main() {
    heap_init(500);
    //Обычное успешное выделение памяти.
    first_test();
    second_test();
    third_test();
    fourth_test();
    fifth_test();
    return 0;
}
